#!/usr/bin/env python
# Filename ecgthreads.py

# Copyright 2016, 2024 Eugen Epure. All Rights Reserved. See the LICENSE file.


import time
import threading
import queue
import sys
import ecggui
import ecgreceive

""" ECG GUI and processing threads """

# number of new samples to read, before processing, updating data and
# strip-chart plot
NSAMPLES = 100


class EcgThreads:
    """
    Launch the main part of the GUI and the worker thread
    """
    def __init__(self, master, ser, flt, bpm):
        """
        Start the GUI and the asynchronous thread, a new thread is
        neededfor the worker.
        """
        self.master = master
        self.ser = ser

        # Create the queue
        self.queue = queue.Queue()

        # Set up the GUI part
        self.gui = ecggui.EcgGui(master, self.queue, flt, bpm,
                                    self.endApplication)

        # Set up the thread to do asynchronous I/O
        try:
            ser.open()
        except Exception as e:
            print("Exception when opening port ", self.ser.port, \
                    " : ", str(e))
            exit()
        
        self.ecgrec = ecgreceive.EcgReceiever(ser)
        self.running = True
        self.thread1 = threading.Thread(target=self.workerThread1)
        self.thread1.start()

        # Start the periodic call in the GUI
        self.periodicCall()

    def periodicCall(self):

        self.gui.processIncoming()
        if not self.running:
            sys.exit(1)

        # recall after 25 ms, to see if there is something to process
        self.master.after(25, self.periodicCall)

    def workerThread1(self):
        """
        Read ECG samples and put them in the queue
        """
        while self.running:
            if self.ecgrec.comm_ok:
                self.queue.put(self.ecgrec.read_samples(NSAMPLES))
            else:
                print("Serial port communication error: comm_ok = False")

    def endApplication(self):
        self.running = False
