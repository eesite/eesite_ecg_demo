#!/usr/bin/env python
# Filename ecggui.py

# Copyright 2016, 2024 Eugen Epure. All Rights Reserved. See the LICENSE file.


"""
ECG demo TK GUI, Matplotlib -- restore background and plot signal
"""

import time
import threading
import random
import queue
from collections import deque

from tkinter import *
from matplotlib.backends.backend_tkagg import FigureCanvasTkAgg

try:
	from matplotlib.backends.backend_tkagg import NavigationToolbar2TkAgg
except ImportError:
	from matplotlib.backends.backend_tkagg import NavigationToolbar2Tk as NavigationToolbar2TkAgg 

from matplotlib.figure import Figure
import matplotlib.ticker as ticker

ECG_BUFFER_SIZE = 1500
PLOT_NPOINTS = 1500

class EcgGui:
    def __init__(self, master, queue, flt, bpm, endCommand):
        self.counter = 0
        self.display = False
        self.queue = queue
        self.plot_buffer = deque(maxlen=ECG_BUFFER_SIZE)
        self.name = "NOT_SET"
        self.x_values = [3.0 * i / PLOT_NPOINTS \
                            for i in range(0, PLOT_NPOINTS)]
        self.y_values = [0.0 for i in range(0, PLOT_NPOINTS)]
        self.filters = flt
        self.bpm = bpm

        # Set up the GUI
        # Plot frame, the matplotlib toolbar doesn't support a grid manager
        self.pframe = Frame(master)
        self.pframe.pack(side=LEFT)#, fill=BOTH, expand=1)
        # Matplotlib initialization
        self.f = Figure(figsize=(8,3), tight_layout=True) # figure
        self.canvas = FigureCanvasTkAgg(self.f, master=self.pframe)
        self.canvas.get_tk_widget().pack()
        self.ax = self.f.add_subplot(111, xlim=(0.0, 3.0), \
                    ylim=(-1.0, 1.0))

        # x axis
        majorXlocator = ticker.MultipleLocator(1.0)
        self.ax.xaxis.set_major_locator(majorXlocator)
        minorXlocator = ticker.MultipleLocator(0.2)
        self.ax.xaxis.set_minor_locator(minorXlocator)
        # y axis
        minorYlocator = ticker.MultipleLocator(0.1)
        self.ax.yaxis.set_minor_locator(minorYlocator)
        self.ax.set_xlabel('Time [s]')
        self.ax.set_ylabel('V [mV]')
        self.ax.grid(True, which='minor', color='0.7',linestyle='-')
        self.canvas.draw()
        # Let's capture the background of the figure
        self.background = self.canvas.copy_from_bbox(self.ax.bbox)
        self.line, = self.ax.plot(self.x_values, self.y_values, \
                        linewidth=1.5) #, color='black')

        self.bpm_text = self.ax.text(0.05, 0.9, '', fontsize=16, \
                                    transform=self.ax.transAxes)

        # Navigation toolbar
        toolbar = NavigationToolbar2TkAgg(self.canvas, self.pframe)
        toolbar.update()
        self.canvas._tkcanvas.pack()
        # Controls frame
        self.controls = Frame(master)
        self.controls.pack()
        Label(self.controls, text="Name", anchor=W).pack(fill=BOTH)
        self.name = StringVar()
        self.name_entry = Entry(self.controls, textvariable=self.name)
        self.name_entry.pack(fill=BOTH)
        Label(self.controls, text=".::.").pack(fill=BOTH)
        self.run_button = Button(self.controls, text='Run', \
                            command=self.run_click)
        self.run_button.pack(fill=BOTH)

    def processIncoming(self):
        """Get ECG samples from queue, if available.
        If the buffer is full, we can update the plot
        """
        bpm_value = 0

        while self.queue.qsize():
            try:
                samples = self.queue.get(0)
            except queue.Empty:
                print("Exception: Queue is empty")
                samples = None

            if self.display:
                # restore plot background
                self.canvas.restore_region(self.background)

                if samples is None: # plot zero values
                    self.plot_buffer.clear()
                    self.line.set_ydata(self.y_values)
                else:
                    # if not None, feed the samples to the queue
                    self.plot_buffer.extend(samples)
                    # if the buffer is full, update plot data
                    if len(self.plot_buffer) == ECG_BUFFER_SIZE:
                        ecg_data = list(self.plot_buffer)

                        # apply the filters as zero phase IIR biquads
                        for f in self.filters:
                            for i in (0, 1):
                                f.clear
                                f.filter(ecg_data)
                                ecg_data.reverse()
                        
                        self.line.set_ydata(ecg_data)
                        self.ax.draw_artist(self.line)

                        bpm_value = self.bpm.get_bpm(ecg_data)
                        bpm_text_string = "BPM: " + str(bpm_value) + \
                                    " Name: " + self.name.get() + \
                                    " | " + \
                                    time.strftime("%Y-%m-%d / %H:%M:%S")

                        self.bpm_text.set_text(bpm_text_string)
                        self.ax.draw_artist(self.bpm_text)
                        
                        self.canvas.blit(self.ax.bbox)                       

                    else:
                        pass
            else:
                self.plot_buffer.clear()

    def run_click(self):
        if self.display:
            self.display = False
            self.name_entry.config(state=NORMAL)
            self.run_button.config(text="Run")
        else:
            self.display = True
            self.name_entry.config(state=DISABLED)
            self.run_button.config(text="Stop")
