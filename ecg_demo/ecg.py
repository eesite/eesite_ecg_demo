#!/usr/bin/env python3
# Filename ecg.py

# Copyright 2016, 2024 Eugen Epure. All Rights Reserved. See the LICENSE file.


"""
Main python script:
    ./ecg.py -p <port> -l <line-frequency>
"""

import tkinter
import serial
import argparse

import ecgreceive
import ecggui
import ecgthreads
import biquad
import ecgbpm


parser = argparse.ArgumentParser(description=\
            "Serial port ECG monitor")

parser.add_argument("-p", "--port", help="Serial port name")

parser.add_argument("-l", "--line", help="Line frequency, \
                        50 or 60 [Hz]", type=int)

args = parser.parse_args()

if (args.line == 50) or (args.line is None):
    # notch filter fmin = 48 Hz, fmax = 52 Hz, fs = 500 Hz
    notch1 = biquad.Biquad(a=(1.0, -1.578856, 0.950957),\
                            b=(1.0, -1.618545, 1.0),\
                            gain=0.975478)

    # notch filter fmin = 95 Hz, fmax = 105 Hz, fs = 500 Hz
    notch2 = biquad.Biquad(a=(1.0, -0.582602, 0.881619),\
                            b=(1.0, -0.619256, 1.0),\
                            gain=0.940809)

elif args.line == 60:
    # notch filter fmin = 58 Hz, fmax = 62 Hz, fs = 500 Hz
    notch1 = biquad.Biquad(a=(1.0, -1.458399, 0.950957),\
                            b=(1.0, -1.422636, 1.0),\
                            gain=0.975478)

    # notch filter fmin = 115 Hz, fmax = 125 Hz, fs = 500 Hz
    notch2 = biquad.Biquad(a=(1.0, -0.118381, 0.881619),\
                            b=(1.0, -0.125829, 1.0),\
                            gain=0.940809)

else:
    print("Wrong option: -l " + str(args.line))
    exit(1)

# LPF fc = 100 Hz, fs = 500 Hz
lpf = biquad.Biquad(a=(1.0, -0.369527, 0.195816),\
                    b=(1.0, 2.0, 1.0),\
                    gain=0.206572)

flt = (notch1, notch2, lpf)

if args.port is None:
    port = "/dev/ttyUSB0"
else:
    port = args.port


ser = serial.Serial()

ser.port = port
ser.baudrate = 19200
ser.bytesize = serial.EIGHTBITS
ser.parity = serial.PARITY_NONE
ser.stopbits = serial.STOPBITS_ONE
ser.timeout = 2
ser.xonxoff = False
ser.rtscts = False
ser.dsrdtr = False

bpm = ecgbpm.EcgBpm(500.0, 0.3, 0.75, 0.7)

root = tkinter.Tk()
root.title("ECG device from http://eesite.bitbucket.io")
root.resizable(0,0) # no maximize control

client = ecgthreads.EcgThreads(root, ser, flt, bpm)

def on_closing():
    client.endApplication()
    root.destroy()

root.protocol("WM_DELETE_WINDOW", on_closing)

root.mainloop()
