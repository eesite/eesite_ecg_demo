#!/usr/bin/env python
# Filename ecgbpm.py

# Copyright 2016 Eugen Epure. All Rights Reserved. See the LICENSE file.


"""
Measures the BPM by analyzing the signal samples. If more signal periods
are present, the BPM values are averaged.
"""

ECG_MIN_BPM = 30
ECG_MAX_BPM = 220

class EcgBpm():
    def __init__(self, fs=500.0, min_ampl=1.0, trig_level=0.75, \
                    guard = 0.5):
        self.fs = fs        
        self.min_ampl = min_ampl
        self.trig_level = trig_level
        self.guard_period = int(fs * guard * 60.0 / ECG_MAX_BPM)

    def get_bpm(self, signal):
        x_min = min(signal)
        x_max = max(signal)
        ampl = x_max-x_min
        trig_indexes = []
        trig_value = x_min + self.trig_level * ampl

        bpm = 0.0

        if ampl > self.min_ampl:

            i = 0
            while i < (len(signal) - 2):
                if (signal[i] < trig_value) and \
                        (signal[i+1] >= trig_value):

                    trig_indexes.append(i)
                    i += self.guard_period

                else:
                    i += 1

            l = len(trig_indexes)

            if l > 1:
                b = 60.0 * self.fs * (l - 1) / \
                        (trig_indexes[l-1] - trig_indexes[0])

                if (b > ECG_MIN_BPM) and (b < ECG_MAX_BPM):
                    bpm = b
                bpm = b

        return int(bpm)
