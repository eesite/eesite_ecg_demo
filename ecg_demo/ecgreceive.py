#!/usr/bin/env python
# Filename ecgreceive.py

# Copyright 2016 Eugen Epure. All Rights Reserved. See the LICENSE file.


"""
Receive the bytes from the serial port, obtain the samples values.
"""

SERIAL_RECEIVE_MAX_TRIES = 5

class EcgReceiever:
    """
    Serial port receiver class
    """
    def __init__(self, ser):
        self.serial_port = ser
        self.comm_ok = True
        self.received_samples = None
        self.reset()

    def reset(self):
        """
        Reset the serial receive stream, to get LSB, MSB in order 
        """
        if self.comm_ok:
            try:
                self.serial_port.flushInput()
                self.serial_port.flushOutput()
            except Exception as e1:
                print("Serial flush exception: " + str(e1))
                self.comm_ok = False

        # receive until we get the MSB, then the LSB should follow
        num_tries = 0
        continue_receiving = True
        while (num_tries < SERIAL_RECEIVE_MAX_TRIES) \
                and continue_receiving and self.comm_ok:
            try:
                read_chr = self.serial_port.read()
            except Exception as e2:
                print("Serial read exception: " + str(e2))
                self.comm_ok = False
                break

            if len(read_chr) == 1:
                if (ord(read_chr) & 0x80) == 0x80: # we've got an MSB
                    continue_receiving = False
            else:
                self.comm_ok = False

            num_tries += 1

        if num_tries == SERIAL_RECEIVE_MAX_TRIES:
            self.comm_ok = False

    def read_samples(self, nsamples):
        """
        Read nsamples ECG samples, process them and return the buffer
        if successful, or None in the case of receive error. 
        In case of serial communication error, try to reset the stream
        or set comm_ok to False.
        """
        try:
            read_bytes = self.serial_port.read(2 * nsamples)
        except Exception as e3:
            print("Serial read exception: " + str(e3))
            self.comm_ok = False

        if self.comm_ok:
            if read_bytes is None:
                self.comm_ok = False

        self.received_samples = None

        if self.comm_ok:
            if len(read_bytes) == (2 * nsamples):
                self.received_samples = []
                for i in range(0, nsamples):
                    lsb = read_bytes[2 * i]
                    msb = read_bytes[2 * i + 1]

                    if ((lsb & 0x80) == 0x00) \
                        and ((msb & 0x80) == 0x80):
                        sample = (lsb & 0x7F) + ((msb & 0x7F) * 128)
                        self.received_samples.append((sample - 8192) * \
                                                        (2.0 / 8192.0))
                    else:    # LSB MSB not in order
                        self.received_samples = None
                        self.reset()
                        break
            else:
                self.comm_ok = False

        return self.received_samples
