#!/usr/bin/env python
# Filename biquad.py

# Copyright 2016 Eugen Epure. All Rights Reserved. See the LICENSE file.


"""
Biquad IIR stage implementation and test
"""


from sys import argv

class Biquad():
    def __init__(self, a=(1.0, 0.0, 0.0), b=(1.0, 0.0, 0.0), gain=1):
        b = (gain * b[0], gain * b[1], gain * b[2])
        self.is_float = isinstance(a[0], float)
        if self.is_float:
            self.a = (1.0, a[1] / a[0], a[2] / a[0])
            self.b = (b[0] / a[0], b[1] / a[0], b[2] / a[0])
            self.clear_value = 0.0
            self.filter = self.filter_df2t # floating point filter
            self.filter_sample = self.filter_df2t_sample
        else:
            self.a = (a[0], a[1], a[2])
            self.b = (b[0], b[1], b[2])            
            self.clear_value = 0
            self.acc = 0
            if a[0] != 0 and (a[0] & (a[0] - 1) == 0):
                self.rshift = a[0].bit_length() - 1 # scaling factor
                self.filter = self.filter_df1_fp_ns # fixed point filter
                self.filter_sample = self.filter_df2t_sample
                self.acc = 0
            else:
                raise Error('a[0] integer is not a power of 2')
        self.clear()

    def filter_df1(self, signal):
        """ Direct form 1, vector """        
        for i in range(0, len(signal)):
            signal[i] = self.filter_df1_sample(signal[i])                                        

    def filter_df1_sample(self, sample):
        """ Direct form 1, sample by sample """
        out_sample = self.b[0] * sample + self.b[1] * self.dl_b[0] + \
                        self.b[2] * self.dl_b[1] - \
                        self.a[1] * self.dl_a[0] - \
                        self.a[2] * self.dl_a[1]                                
        self.dl_a[1] = self.dl_a[0]
        self.dl_a[0] = out_sample            
        self.dl_b[1] = self.dl_b[0]
        self.dl_b[0] = sample
        return out_sample

    def filter_df1_fp_ns(self, signal):
        """ Direct form 1, fixed point, noise shaping, vector """
        for i in range(0, len(signal)):
            signal[i] = self.filter_df1_fp_ns_sample(signal[i])

    def filter_df1_fp_ns_sample(self, sample):
        """ Direct form 1, fixed point, noise shaping, sample """
        self.acc += self.b[0] * sample + self.b[1] * self.dl_b[0] + \
                self.b[2] * self.dl_b[1] - \
                self.a[1] * self.dl_a[0] - \
                self.a[2] * self.dl_a[1]
        out_sample = self.acc >> self.rshift
        self.acc -= out_sample << self.rshift # quantization error
        self.dl_a[1] = self.dl_a[0]
        self.dl_a[0] = out_sample
        self.dl_b[1] = self.dl_b[0]
        self.dl_b[0] = sample        
        return out_sample

    def filter_df2(self, signal):
        """ Direct form 2, vector """
        for i in range(0, len(signal)):
            signal[i] = self.filter_df2_sample(signal[i])

    def filter_df2_sample(self, sample):
        """ Direct form 2 sample by sample """
        a_sum = sample - self.a[1] * self.dl_a[0] - \
                    self.a[2] * self.dl_a[1]

        sample = self.b[0] * a_sum + self.b[1] * self.dl_a[0] + \
                        self.b[2] * self.dl_a[1]

        self.dl_a[1] = self.dl_a[0]
        self.dl_a[0] = a_sum
        return sample

    def filter_df2t(self, signal):
        """ Direct form 2 transposed, vector """
        for i in range(0, len(signal)):
            signal[i] = self.filter_df2t_sample(signal[i])

    def filter_df2t_sample(self, sample):
        """ Direct form 2 transposed, sample by sample """        
        out_sample = self.b[0] * sample + self.dl_a[0]
        self.dl_a[0] = self.b[1] * sample + self.dl_a[1] - \
                        self.a[1] * out_sample
        self.dl_a[1] = self.b[2] * sample - \
                        self.a[2] * out_sample
        return out_sample

    def clear(self):
        """ Clear biquad state """
        self.dl_a = [self.clear_value, self.clear_value]
        self.dl_b = [self.clear_value, self.clear_value]

# tests
if __name__ == '__main__':    
    [test_type, N] = argv[1:]

    print(N)

    a = (1.0000000000000000e+0, -1.9161349255980247e+0, \
            9.3906250581749240e-001)
    b = (1.0000000000000000e+0,  0.0000000000000000e+0, \
            -1.0000000000000000e+0)
    g = 3.0468747091253801e-2;
    print("Testing Butterworth BPF biquad: f1 = 2 Hz, f2 = 3 Hz, " + \
            "fs = 100 Hz with:")
    print("\ta = ", a)
    print("\tb = ", b, ", gain = ", g)

    if test_type == "int16":
        print("int16 step response: Input = 32767")
        samples = int(N) * [pow(2,14) - 1]
        a = (int(16384 * a[0]), int(16384 * a[1]), int(16384 * a[2]))
        b = (int(16384 * g * b[0]), int(16384 * g * b[1]), \
                int(16384 * g * b[2]))
        bq = Biquad(a, b)
        bq.filter(samples)
    elif test_type == "int32":
        print("int32 step response: Input = 2^31 - 1")
        samples = int(N) * [pow(2, 31) - 1]
        a = (int(pow(2, 30) * a[0]), int(pow(2, 30) * a[1]), \
                int(pow(2, 30) * a[2]))
        b = (int(pow(2, 30) * g * b[0]), int(pow(2, 30) * g * b[1]), \
                int(pow(2, 30) * g * b[2]))
        bq = Biquad(a, b)
        bq.filter(samples)
    elif test_type == "float":
        print("float (DF2T) step response: Input = 1.0")
        samples = int(N) * [1.0]
        bq = Biquad(a, b, g)
        bq.filter(samples)
    elif test_type == "floatDF1":
        print("float DF1 step response: Input = 1.0")
        samples = int(N) * [1.0]
        bq = Biquad(a, b, g)
        bq.filter_df1(samples)
    elif test_type == "floatDF2":
        print("float DF2 step response: Input = 1.0")
        samples = int(N) * [1.0]
        bq = Biquad(a, b, g)
        bq.filter_df2(samples)
    else:
        print("Specify test type: int16/int32/float/floatDF1/floatDF2")
        exit   

    print("Output, first ", int(N), " steps:")
    for i in range(0, len(samples)):
        print(samples[i])
