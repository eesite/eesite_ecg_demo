/*
 * ecg.c
 *
 * Created: 6/28/2016 23:01:06
 *  Author: Eugen Epure
 * Copyright 2016 Eugen Epure. All Rights Reserved. See the LICENSE file.
 */ 


#include <avr/io.h>
#include <avr/sfr_defs.h>
#include <avr/interrupt.h>
#include <avr/wdt.h>
#include <stdint.h>

#define NAVG 16 // number of averaged samples, to obtain maximum 14 bits
#define NDIV 1 // division constant to get a 14 bits sample

// Constant for 19200 baud UART bit delay: DELAY(uint16_t, UART_BIT_DELAY)
//		at (16 - 0.38) MHz, gives a ~ 52 us delay, approx 19231 baud
#define UART_BIT_DELAY 134
#define UART_INIT_DELAY 52065 // approx. 20 ms
#define UART_STOP_DELAY 520 // 200 us
#define UART_TX_PIN PB0
#define UART_TX_PORT PORTB
#define UART_TX_DDR DDRB
#define True 1
#define False 0

// delay macro: DELAY(uint8_t, n) = (3 * n + 3) cycles
//				DELAY(uint16_t, n) = (6 * n + 8) cycles
//				DELAY(uint32_t, n) = (10 * n + 13) cycles
#define DELAY(u, n) do { u = n; while(u--); } while(0)

register uint16_t acc asm("r2");
register uint16_t result asm("r4");
register uint8_t sample_num asm("r6");
register uint8_t new_data asm("r7");


ISR(ADC_vect) { // ISR interrupt at 8 kHz x 47 cycles ~ 380 kIPS
	
	// ADC is triggered on Compare Match A
	// clear the timer compare match flag
	TIFR0 = _BV(OCF0A);
	
	// accumulate samples values
	acc += ADCW;
	sample_num++;
	
	if( !(sample_num % NAVG) ) {
		// result is ready
		result = acc / NDIV;		
		new_data = True;
		acc = 0; // zero the accumulator for the next cycle			
	}	
}

void uart_send_byte(uint8_t b) {
	uint8_t mask[] = {0x01, 0x02, 0x04, 0x08, 0x10, 0x20, 0x40, 0x80};
	uint8_t i;
	uint16_t u16;
	
	UART_TX_PORT |= _BV(UART_TX_PIN); // start bit, UART space
	DELAY(u16, UART_BIT_DELAY);	

	for(i=0; i<8; i++) { // send data bits
		if(b & mask[i]) {
			UART_TX_PORT &= ~_BV(UART_TX_PIN); // UART mark
		}
		else {
			UART_TX_PORT |= _BV(UART_TX_PIN); // UART space
		}
		
		DELAY(u16, UART_BIT_DELAY);		
	}
	
	UART_TX_PORT &= ~_BV(UART_TX_PIN); // UART mark
	DELAY(u16, UART_STOP_DELAY);	// stop bits/idle state
}

void init() {
	uint16_t u16;
	ADMUX = _BV(MUX1); // ADC2 input	
	// ADC auto-trigger, with interrupt enabled, f = 250 kHz
	ADCSRA = _BV(ADEN) | _BV(ADATE) |_BV(ADIE) | _BV(ADPS2) | _BV(ADPS1);
	// ADC trigger on Compare Match A
	ADCSRB = _BV(ADTS1) | _BV(ADTS0);
	// disable the digital input on ADC2
	DIDR0 = _BV(ADC2D);
	// Timer0, CTC
	TCCR0A = _BV(WGM01);
	TCCR0B = _BV(CS01); // prescaler=8
	OCR0A = 249; // 16 MHz / 8 / 250 = 8 kHz
	// UART output pin
	UART_TX_DDR |= _BV(UART_TX_PIN);
	UART_TX_PORT &= ~_BV(UART_TX_PIN); // UART mark, default state
	DELAY(u16, UART_INIT_DELAY); // keep UART in mark state to initialize properly		
}

int main(void)
{	
	wdt_reset();
	acc = 0;
	result = 0;
	sample_num = 0;
	new_data = False;
	
	init();
	
	wdt_enable(WDTO_1S);	
	
	// enable interrupts
	sei();
	
	for(;;) {
		if(new_data) {
			uart_send_byte(result & 0x007F); // send lower byte
			uart_send_byte((result >> 7) | 0x80); // MSB is 1, to indicate the higher byte
			new_data = False;
		}
		
		wdt_reset();		
	}
}
