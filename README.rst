===============
eesite ECG demo
===============

.. warning:: A home made ECG device can be very dangerous! Any person who builds and/or uses a device like this, takes the entire responsability for the consequences!

**December 2024:** Updated for Python 3.11 version


https://eesite.bitbucket.io/html/electronics/ecg2/ecg2.html

This ECG monitor consists of a 2 electrode interface that connects to the PC serial port and \
a Python program that does the ECG signal processing, displays the waveform and the BPM value,
you can see it in action here: https://www.youtube.com/watch?v=RQHX3KXWS_0 .


---------------------------
Two electrode ECG interface
---------------------------

The schematic is in the **hardware** directory, and the microcontroller code can be found in the
**firmware** directory.


---------------------------
Python ECG monitor software
---------------------------

.. image:: ecg2_img.png

**Usage**::

	./ecg.py -p <port> -l <line-frequency>


**Options**
	-p  serial port name
	-l  line frequency, 50 or 60


-----
About
-----

Written by Eugen Epure, see the LICENSE file.
